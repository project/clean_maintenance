CONTENTS OF THIS FILE
--------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------

Clean maintenance is a simple module that changes the look and feel of the
maintenance page.

With this module there is no need for a custom design or coding.

 * For a full description of the module visit:
   https://www.drupal.org/project/clean_maintenance

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/clean_maintenance


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Clean maintenance module as you would normally install a
   contributed Drupal module.

   Visit https://www.drupal.org/docs/7/extend/installing-modules for further
   information.


CONFIGURATION
-------------

This module requires no configuration changes.


TROUBLESHOOTING
---------------

If the module is not shown in the list try deleting the module and try cloning
it again, or else try clearing the cache, and then try installing it.


MAINTAINERS
-----------

 * Erik de Kamps - https://www.drupal.org/u/erik-de-kamps

Supporting organization:

 * Lucius Digital - https://www.drupal.org/lucius-digital

