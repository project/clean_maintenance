<?php
/**
 * @file
 * Theme implementation to display the miantenance page.
 */
?>

<!doctype html>
<title>Site Maintenance</title>
<style>
  body { text-align: center; padding: 150px; }
  h1 { font-size: 50px; }
  body { font: 20px Helvetica, sans-serif; color: #333; }
  article { display: block; text-align: left; width: 650px; margin: 0 auto; }
  a { color: #dc8100; text-decoration: none; }
  a:hover { color: #333; text-decoration: none; }
</style>

<article>
  <h1><?php print t('We&rsquo;ll be back soon!'); ?></h1>
  <div>
    <p><?php print (!empty($maintenance_text) ? $maintenance_text : ''); ?></p>
    <p>&mdash; <?php print (!empty($site_name) ? $site_name : ''); ?></p>
  </div>
</article>
